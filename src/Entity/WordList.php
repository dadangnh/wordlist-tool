<?php

namespace App\Entity;

use App\Repository\WordListRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Wordlist entity
 * Consist of plain word, md5 hash, and sha1 hash
 */
#[ORM\Entity(
    repositoryClass: WordListRepository::class,
)]
#[UniqueEntity(
    "word",
    message: "this word is already in database"
)]
#[ORM\Table(
    name: "word_list",
)]
#[ORM\Index(
    columns: ["word"],
    name: "idx_word"
)]
#[ORM\Index(
    columns: ["md5"],
    name: "idx_hash_md5"
)]
#[ORM\Index(
    columns: ["word", "md5"],
    name: "idx_both_md5"
)]
#[ORM\Index(
    columns: ["sha1"],
    name: "idx_hash_sha1"
)]
#[ORM\Index(
    columns: ["word", "sha1"],
    name: "idx_both_sha1"
)]
class WordList
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(
        type: "integer"
    )]
    private $id;

    #[ORM\Column(
        type: "string",
        length: 255,
        unique: true,
        nullable: true
    )]
    private $word;

    #[ORM\Column(
        type: "string",
        length: 32
    )]
    #[Assert\NotNull]
    private $md5;

    #[ORM\Column(
        type: "string",
        length: 40
    )]
    #[Assert\NotNull]
    private $sha1;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(string $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getMd5(): ?string
    {
        return $this->md5;
    }

    public function setMd5(string $md5): self
    {
        $this->md5 = $md5;

        return $this;
    }

    public function getSha1(): ?string
    {
        return $this->sha1;
    }

    public function setSha1(string $sha1): self
    {
        $this->sha1 = $sha1;

        return $this;
    }
}

<?php

namespace App\Repository;

use App\Entity\WordList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WordList|null find($id, $lockMode = null, $lockVersion = null)
 * @method WordList|null findOneBy(array $criteria, array $orderBy = null)
 * @method WordList[]    findAll()
 * @method WordList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WordListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WordList::class);
    }

    /**
     * Method to find a wordlist hash by a word
     * @param string $word
     * @return WordList|null
     */
    public function findHashByWord(string $word): ?WordList
    {
        try {
            return $this->createQueryBuilder('w')
                ->andWhere('w.word = :val')
                ->setParameter('val', $word)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * Method to find the wordlist (word) from md5 hash
     * @param string $hash
     * @return WordList|null
     */
    public function findHashByMd5(string $hash): ?WordList
    {
        try {
            return $this->createQueryBuilder('w')
                ->andWhere('w.md5 = :val')
                ->setParameter('val', $hash)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * Method to find Wordlist (word) from a sha1 hash
     * @param string $hash
     * @return WordList|null
     */
    public function findHashBySha1(string $hash): ?WordList
    {
        try {
            return $this->createQueryBuilder('w')
                ->andWhere('w.sha1 = :val')
                ->setParameter('val', $hash)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * Method to get a list of wordlist from data id: $id with a limit of: $limit
     * @param int $id
     * @param int $limit
     * @return int|mixed|string
     */
    public function getStartLimit(int $id, int $limit): mixed
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.id >= :start')
            ->setParameter('start', $id)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * Method to find a wordlist from id: $id
     * @param int $id
     * @return int|mixed|string
     */
    public function getStartById(int $id): mixed
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.id >= :start')
            ->setParameter('start', $id)
            ->orderBy('w.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

}

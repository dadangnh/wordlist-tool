<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app')]
class AppCommand extends Command
{
    /**
     * Initialize the cli command information
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Application to convert a word into md5 hash')
        ;
    }

    /**
     * Execution method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Initialize symfony styler
        $io = new SymfonyStyle($input, $output);

        // create information of the available command
        $header = ['Command', 'Description'];
        $command = [
            ['app:show', 'Show available wordlist data from database'],
            ['app:process', 'Convert a word/ wordlist into hashed password'],
            ['app:find', 'Find a hash from a word/ find a word from a hash']
        ];

        // display it
        $io->section('List of available command.');
        $io->table($header, $command);

        return 0;
    }
}

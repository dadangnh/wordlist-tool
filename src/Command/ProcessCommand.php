<?php

namespace App\Command;

use App\Entity\WordList;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\Statement;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

#[AsCommand(name: 'app:process')]
class ProcessCommand extends Command
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    /**
     * Initialize the cli command information
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Process convert wordlist into hash')
            ->addArgument('path_to_file', InputArgument::REQUIRED, 'Path to wordlist file')
        ;
    }

    /** Execution method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // instantiation of symfony style
        $io = new SymfonyStyle($input, $output);

        // get user input
        $path = $input->getArgument('path_to_file');

        // check whether file is exist and readable
        $fs = new Filesystem();
        if (!$fs->exists($path)) {
            $io->error('File is not exist or no permission');
            return 0;
        }

        // Starting the process
        $io->title('Wordlist processor');

        // read the file and convert it as csv
        $reader = Reader::createFromPath($path, 'r');
        try {
            $reader->setHeaderOffset(0);
        } catch (Exception $e) {
            return 0;
        }
        try {
            $data = Statement::create()->process($reader);
        } catch (Exception $e) {
            return 0;
        }

        // get first row, because wordlist consist of 1 word per row, so we take the header as well
        $firstRow = $data->getHeader()[0];

        // count of total wordlist that user imported
        $dataCount = count($data) + 1;

        // start the progress
        $io->progressStart($dataCount);
        // process first row
        if ($firstRow) {
            $wordlist = $this->addWord($firstRow);
            if ($wordlist instanceof WordList) {
                $io->progressAdvance();
            }
        }
        // process the next rows
        foreach ($data as $word) {
            $wordlist = $this->addWord($word[$firstRow]);
            if ($wordlist instanceof WordList) {
                $io->progressAdvance();
            }
        }

        // show the success
        $io->progressFinish();
        $io->success(sprintf('Total number of data: %d', $dataCount));

        return 0;
    }

    /** Method to insert word into wordlist database
     * @param string $word
     * @return WordList
     */
    private function addWord(string $word): WordList
    {
        // check whether word already in database
        $check = $this->em->getRepository('App:WordList')
            ->findOneBy(['word' => $word]);

        // if not in database, import the data
        if (null === $check) {
            $wordlist = new WordList();
            $wordlist->setWord($word);
            $wordlist->setMd5(md5($word));
            $wordlist->setSha1(sha1($word));
            $this->em->persist($wordlist);
            $this->em->flush();

            return $wordlist;
        }

        return $check;
    }
}

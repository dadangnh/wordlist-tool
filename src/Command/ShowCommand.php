<?php

namespace App\Command;

use App\Entity\WordList;
use App\Repository\WordListRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:show')]
class ShowCommand extends Command
{
    /**
     * @var WordListRepository
     */
    private WordListRepository $wordlistRepository;

    public function __construct(WordListRepository $wordListRepository)
    {
        parent::__construct();
        $this->wordlistRepository = $wordListRepository;
    }

    /**
     * Initialize the cli command information
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Show all wordlist data from database')
            ->addArgument('start_id', InputArgument::OPTIONAL, 'Viewing data started from this id')
            ->addArgument('count', InputArgument::OPTIONAL, 'Number of data to display')
        ;
    }

    /** Execution method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Initialize symfony styler
        $io = new SymfonyStyle($input, $output);

        // Get user input
        $startId = $input->getArgument('start_id');
        $count = $input->getArgument('count');

        // Check for data type
        $this->checkDataType($io, 'start_id', $startId);
        $this->checkDataType($io, 'data count', $count);

        // If user set the starting ID
        if ($startId) {
            // if user add maximum number of data
            if ($count) {
                $io->note(sprintf('You want to see data from id: %d for a total of %d data', $startId, $count));
                $this->showData($io, $this->wordlistRepository->getStartLimit($startId, $count));
            } else {
                $io->note(sprintf('You want to see data from id: %d', $startId));
                $this->showData($io, $this->wordlistRepository->getStartById($startId));
            }

        // get all data
        } else {
            $io->note('Viewing all data');
            $this->showData($io, $this->wordlistRepository->findAll());
        }

        return 0;
    }

    /**
     * Method to show the data
     * @param SymfonyStyle $io
     * @param array $data
     */
    private function showData(SymfonyStyle $io, array $data): void
    {
        // Initialize default header and number
        $no = 0;
        $header = ['No', 'ID', 'Word', 'Md5', 'SHA1'];
        $display = [];

        // display
        $io->section('Getting the data....');
        $io->progressStart(count($data));
        foreach ($data as $word) {
            if ($word instanceof WordList) {
                $no++;
                $display[] = [$no, $word->getId(), $word->getWord(), $word->getMd5(), $word->getSha1()];
                $io->progressAdvance();
            }
        }
        $io->progressFinish();
        $io->table($header, $display);
        $io->success(sprintf('Total data processed: %d', $no));
    }

    /**
     * Method to check user input based
     * @param SymfonyStyle $io
     * @param string $type
     * @param $input
     */
    private function checkDataType(SymfonyStyle $io, string $type, $input): void
    {
        // Make sure user only input a numeric value, otherwise throw error
        if (null !== $input && !is_numeric($input)) {
            $io->error(sprintf('Please only specify a number for %s', $type));
            exit();
        }
    }
}

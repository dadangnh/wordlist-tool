<?php

namespace App\Command;

use App\Entity\WordList;
use App\Repository\WordListRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:show')]
class FindCommand extends Command
{
    /**
     * @var WordListRepository
     */
    private WordListRepository $wordListRepository;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    public function __construct(WordListRepository $wordListRepository, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->wordListRepository = $wordListRepository;
        $this->em = $em;
    }

    /**
     * Initialize the cli command information
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Search word/ hash from word/ hash')
            ->addArgument('word_or_hash', InputArgument::REQUIRED, 'The word or hash you want to search')
            ->addOption('type', null, InputOption::VALUE_REQUIRED, 'Type of input data to process, [word, md5, sha1]')
        ;
    }

    /**
     * Execution method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Initialize the symfony cli style
        $io = new SymfonyStyle($input, $output);

        // get data from user input
        $keyword = $input->getArgument('word_or_hash');
        $type = strtolower($input->getOption('type'));

        // check whether user input is correct
        if ('' === $type) {
            $io->error('Please specify the type by using --type=TYPE [word, md5, sha1]');
            return 0;
        }

        // initialize wordlist data
        $wordlist = null;

        // find the wordlist by input type
        if ('word' === $type) {
            $wordlist = $this->wordListRepository->findHashByWord($keyword);

            // if wordlist is not exist, create one
            if (null === $wordlist) {
                $wordlist = new WordList();
                $wordlist->setWord($keyword);
                $wordlist->setMd5(md5($keyword));
                $wordlist->setSha1(sha1($keyword));
                $this->em->persist($wordlist);
                $this->em->flush();
            }

        } elseif ('md5' === $type) {
            $wordlist = $this->wordListRepository->findHashByMd5($keyword);
        } elseif ('sha1' === $type) {
            $wordlist = $this->wordListRepository->findHashBySha1($keyword);
        }

        // if no wordlist data found, send an note
        if (null === $wordlist) {
            $io->note(sprintf('Failed to find data from %s string of %s', $type, $keyword));
            return 0;
        }

        // success note, show the data
        $io->success(sprintf('Wordlist found for %s [%s]', $keyword, $type));
        $io->table(['Word', 'MD5', 'SHA1'], [[$wordlist->getWord(), $wordlist->getMd5(), $wordlist->getSha1()]]);

        return 0;
    }
}

# Wordlist Tool

Script to convert a word/ wordlist into databases of hashed password. Currently, we support md5 and sha1 by default, but you can add more hashing function as much as you want by adding the hash algorithms to Wordlist Entity and ProcessCommand.

## Canonical source

The canonical source of Wordlist Tool where all development takes place is [hosted on GitLab.com](https://gitlab.com/dadangnh/wordlist-tool).

## Requirements

To use this tool, you need:
*  PHP Runtime | minimum version supported is version 8.1, latest version is recommended.
*  Database server | we use [PostgreSQL 14](https://www.postgresql.org/), but you can use any databases supported by [Doctrine Project](https://www.doctrine-project.org/projects/doctrine-dbal/en/current/reference/introduction.html).
*  [Composer](https://getcomposer.org/download/).
*  [Symfony CLI](https://symfony.com/download) (Optional but recommended)

## Installation

First, clone this repository:

```bash
$ git clone git@gitlab.com:dadangnh/wordlist-tool.git
```

Then, create your local environment by editing `.env` and save as `.env.local` or you can use OS's environment variable or use [Symfony Secrets](https://symfony.com/doc/current/configuration/secrets.html)

After that, from inside your project directory, install the required package:

```bash
$ composer install
```

Then, run:
```bash
$ symfony console doctrine:database:create
```
> Note: if you didn't install [Symfony CLI](https://symfony.com/download), simply change the `symfony console` to `php bin/console`


If you use PostgreSQL, you can skip this, if your database is not PostgreSQL, run the following code:
```bash
$ symfony console make:migration
```

Lastly, run the migration:
```bash
$ symfony console doctrine:migrations:migrate
```

Now your app are ready to use:
```bash
$ symfony console app
```

## Contributing

This is an open source project, and we are very happy to accept community contributions.

# License

This code is published under [MIT License](LICENSE.md).

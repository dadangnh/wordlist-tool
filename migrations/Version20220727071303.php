<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220727071303 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE word_list_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE word_list (id INT NOT NULL, word VARCHAR(255) DEFAULT NULL, md5 VARCHAR(32) NOT NULL, sha1 VARCHAR(40) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4C5DFBF5C3F17511 ON word_list (word)');
        $this->addSql('CREATE INDEX idx_word ON word_list (word)');
        $this->addSql('CREATE INDEX idx_hash_md5 ON word_list (md5)');
        $this->addSql('CREATE INDEX idx_both_md5 ON word_list (word, md5)');
        $this->addSql('CREATE INDEX idx_hash_sha1 ON word_list (sha1)');
        $this->addSql('CREATE INDEX idx_both_sha1 ON word_list (word, sha1)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE word_list_id_seq CASCADE');
        $this->addSql('DROP TABLE word_list');
    }
}
